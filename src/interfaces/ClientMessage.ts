import { Vector3 } from "../classes/Vector3";


interface UpdateMessage {
  action: "UPDATE",
  position: Vector3,
  rotation: Vector3,
}


/**
 * Every message format a client can send to this server.
 */
export type ClientMessage = UpdateMessage;