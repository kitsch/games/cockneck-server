import { PlayerData } from "../classes/Player";


interface SetPhantoms {
  action: "SET_PHANTOMS";
  phantoms: PlayerData[];
}


/**
 * Every message format this server can send to a client.
 */
export type ServerMessage = SetPhantoms;