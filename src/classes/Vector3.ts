/**
 * Simple container for 3-dimensional data.
 */
export class Vector3 {
  constructor(public x: number, public y: number, public z: number) {}

  /**
   * Calculates the 3-dimensional difference between a given Vector3.
   * @param goal Vector with which to compare
   * @returns Vector3
   */
  public getDifference(goal: Vector3): Vector3 {
    return new Vector3(
      Math.abs(this.x - goal.x),
      Math.abs(this.y - goal.y),
      Math.abs(this.z - goal.z)
    );
  }

  /**
   * Calculates the straight-line distance between a given Vector3.
   * @param goal Vector with which to compare
   * @returns number
   */
  public getDistance(goal: Vector3): number {
    const diff = this.getDifference(goal);
    return Math.sqrt(diff.x ** 2 + diff.z ** 2);
  }
}