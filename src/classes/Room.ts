import { Player, PlayerData } from "./Player";
import { Vector3 } from "./Vector3";

export const MAX_SLOT_COUNT = 25;
export const UPDATE_INTERVAL = 1000.0 / 10.0;

/**
 * A collection of players which are in the same world.
 */
export class Room {
  protected slots: Player[] = [];

  /**
   * Add a player to an available slot.
   * @param player Object to fill a slot with.
   * @returns Whether or not a slot was filled.
   */
  public add(player: Player): boolean {
    if (this.isFull()) return false;
    if (this.isEmpty()) {
      setInterval(() => this.update(), UPDATE_INTERVAL);
    }

    this.slots.push(player);
    return true;
  }

  /**
   * Returns the general "middle point" of all slots.
   * @returns The average position of all slots.
   */
  public getAveragePosition(): Vector3 {
    const accumulator = new Vector3(0, 0, 0);
    const usedSlotCount = this.getUsedSlotCount();

    for (const slot of this.slots) {
      accumulator.x += slot.position.x;
      accumulator.z += slot.position.z;
    }

    return new Vector3(
      accumulator.x / usedSlotCount,
      0,
      accumulator.z / usedSlotCount,
    );
  }

  /**
   * Returns how many slots are currently being used.
   * @returns Number of used slots.
   */
  public getUsedSlotCount(): number {
    return this.slots.length;
  }

  /**
   * Returns true if the room is empty.
   * @returns Whether or not the room is empty.
   */
  public isEmpty(): boolean {
    return this.getUsedSlotCount() === 0;
  }

  /**
   * Returns true if the room is full.
   * @returns Whether or not the room is full.
   */
  public isFull(): boolean {
    return this.getUsedSlotCount() === MAX_SLOT_COUNT;
  }

  /**
   * Removes a player from the slot list.
   * @param player Player to remove.
   * @returns Whether or not a player was removed.
   */
  public remove(player: Player): boolean {
    // Can't remove if empty.
    if (this.isEmpty()) return false;

    const index = this.slots.findIndex((slot) => slot.id === player.id);

    // Can't remove if not found.
    if (index === -1) return false;

    this.slots.splice(index, 1);
    return true;
  }

  protected update(): void {
    const matches: {[key: string]: string[]} = {};

    const formMatch = (firstIndex: string, secondIndex: string) => {
      if (!matches[firstIndex]) matches[firstIndex] = [];
      if (!matches[secondIndex]) matches[secondIndex] = [];

      matches[firstIndex].push(secondIndex);
      matches[secondIndex].push(firstIndex);
    }

    // Build list of player data.
    const playerDataList: PlayerData[] = this.slots.map(slot => ({
      id: slot.id,
      position: {
        x: slot.position.x,
        y: slot.position.y,
        z: slot.position.z,
      }
    }));

    // Update all players with phantoms.
    for (const slot of this.slots) {
      slot.sendToSocket({
        action: "SET_PHANTOMS",
        phantoms: playerDataList.filter((item) => item.id !== slot.id),
      });
    }
  }
}