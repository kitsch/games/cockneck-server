import { WebSocket } from "ws";
import { ServerMessage } from "../interfaces/ServerMessage";
import { Vector3 } from "./Vector3";

/**
 * Player data sent to the client.
 */
export interface PlayerData {
  id: string;
  position: {
    x: number;
    y: number;
    z: number;
  };
}

/*
 * Container for player information.
 */
export class Player {
  public lastSeen: number = Date.now();

  constructor(
    public id: string,
    public position: Vector3,
    protected socket: WebSocket,
  ) {}

  /**
   * Send data to the client using the socket.
   * @param data A server message to the client.
   */
  public sendToSocket(data: ServerMessage) {
    // If the socket isn't ready, don't send anything.
    if (this.socket.readyState !== this.socket.OPEN) return;

    this.socket.send(JSON.stringify(data));
  }

  /**
   * Sets the last seen value to the current time.
   */
  public updateLastSeen() {
    this.lastSeen = Date.now();
  }
}