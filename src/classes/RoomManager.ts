import { Player } from "./Player";
import { MAX_SLOT_COUNT, Room } from "./Room";

/**
 * Controller for the lifecycle of rooms and moving players between them.
 */
export class RoomManager {
  protected rooms: Room[] = [];

  /**
   * Adds a player to whichever room is determined to be best.
   * @param player New player object to add.
   */
  public add(player: Player): void {
    let suitableRoom: Room;

    // Determine a suitable room for the new player.
    for (const room of this.rooms) {
      if (room.isFull()) continue;

      suitableRoom = room;
      break;
    }

    // If no suitable room, create one.
    if (!suitableRoom) {
      suitableRoom = new Room();
      this.rooms.push(suitableRoom);
    }

    suitableRoom.add(player);
  }

  /**
   * Cleans a room. (ie. enacts if it should be destroyed and migrated)
   * @param room Room to do cleaning of.
   * @returns void
   */
  private cleanRoom(room: Room): void {
    const usedSlotCount = room.getUsedSlotCount();

    // Do not clean up if we only have one room.
    if (this.rooms.length === 1) return;

    // Do not clean up if we have over 1/5th of the maximum slots filled.
    if (usedSlotCount > MAX_SLOT_COUNT * .2) return;
  }

  /**
   * Removes a player from any room.
   * @param player Player to remove.
   * @returns Whether or not a player was removed.
   */
  public remove(player: Player): boolean {
    return this.rooms.some((room) => room.remove(player));
  }
}