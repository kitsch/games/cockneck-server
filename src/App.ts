import { createServer } from "http";
import { RawData, WebSocket, WebSocketServer } from 'ws';
import { Player } from "./classes/Player";
import { RoomManager } from "./classes/RoomManager";
import { Vector3 } from "./classes/Vector3";
import { getUID } from "./functions/generateId";
import { ClientMessage } from "./interfaces/ClientMessage";


/**
 * The entry point for the Cockneck server.
 */
export class App {
  protected roomManager = new RoomManager();

  constructor(protected port: number) {
    const server = createServer();
    const wss = new WebSocketServer({ server });

    wss.on("connection", (ws) => this.onConnection(ws));

    console.log(`Starting on port ${port}`);
    server.listen(port);
  }

  /**
   * Puts a player into our system after they connect.
   * @param ws The new WebSocket, connecting to an individual player.
   */
  protected onConnection(ws: WebSocket) {
    const player = new Player(getUID(), new Vector3(0, 0, 0), ws);

    this.roomManager.add(player);
    console.log("Player joined");

    ws.on("message", (rawData) => this.onMessage(player, rawData));
    ws.on("close", () => {
      this.roomManager.remove(player);
      console.log("Player disconnected");
    });
  }

  /**
   * Interprets and acts on a message from a player.
   * @param player The player who sent the message.
   * @param rawData Data received from player.
   */
  protected onMessage(player: Player, rawData: RawData) {
    const data = JSON.parse(rawData.toString()) as ClientMessage;

    switch (data.action) {
      case "UPDATE":
        player.position = data.position;
        break;
    }
  }
}