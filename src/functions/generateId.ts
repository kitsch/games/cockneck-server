var id = 1000;

export function getUID(): string {
  return `ID-${++id}`;
}